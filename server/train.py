import csv
import numpy as np
import tensorflow as tf
import tensorflow_hub as hub
import os

from sklearn import utils
from tensorflow import keras
from tensorflow.keras import layers

# Directory for the Kaggle data to train on
data_dir = '../data'

# Directory where the trained model has to be saved
models_dir = '../model'


def get_model():
    # Handles for the TF Hub modules
    dan_encoder_handle = "https://tfhub.dev/google/universal-sentence-encoder/2"
    transformer_encoder_handle = "https://tfhub.dev/google/universal-sentence-encoder-large/3"

    dan_encoder = hub.Module(dan_encoder_handle)
    transformer_encoder = hub.Module(transformer_encoder_handle)

    # Returns the concatenated sentence embeddings produced by the two hub modules
    def sentence_encoder_layer(x):
        x = tf.reshape(tf.cast(x, tf.string), [-1])
        x = layers.concatenate([
            dan_encoder(x), transformer_encoder(x)
        ])

        return x

    input_sentences = layers.Input(shape=(1,), dtype=tf.string)
    x = layers.Lambda(lambda x: sentence_encoder_layer(x), output_shape=(1024,))(input_sentences)

    # Apply a few fully connected layers to get the final prediction
    x = layers.Dense(256, activation='relu')(x)
    x = layers.Dense(64, activation='relu')(x)
    x = layers.Dense(1, activation='sigmoid')(x)

    model = keras.Model(inputs=[input_sentences], outputs=x)
    model.compile(loss='binary_crossentropy', metrics=['accuracy'], optimizer='sgd')

    return model


def wrap_string_list(lst):
    return np.array(lst, dtype=object)[:, np.newaxis]


def load_csv(path):
    with open(path) as f:
        reader = csv.reader(f)
        return list(reader)


# Load the Kaggle data file
def load_data(path):
    xs = []
    ys = []

    for line in load_csv(path):
        sentence = line[1]
        labels = line[2:]

        xs.append(sentence)
        ys.append(int('1' in labels))

    xs = wrap_string_list(xs)
    return xs, ys


# Get weights for training on data with unbalanced classes
def get_class_weights(ys):
    class_weights = utils.class_weight.compute_class_weight(
        'balanced', np.unique(ys), ys)
    class_weights = dict(enumerate(class_weights))

    return class_weights


# Save the model in a format that can be used by TensorFlow serving
def save_model(model, session, path):
    model_version = 1

    tf.saved_model.simple_save(
        session,
        os.path.join(path, str(model_version)),
        inputs={'sentences': model.input},
        outputs={t.name: t for t in model.outputs},
        legacy_init_op=tf.tables_initializer())


if __name__ == "__main__":
    model = get_model()
    print(model.summary())

    session = tf.Session(graph=tf.get_default_graph())

    # Initialize TensorFlow
    keras.backend.set_session(session)
    session.run(tf.global_variables_initializer())
    session.run(tf.tables_initializer())

    x_train, y_train = load_data(os.path.join(data_dir, 'train.csv'))

    weights = get_class_weights(y_train)
    model.fit(x_train, y_train, epochs=4, class_weight=weights)

    save_model(model, session, models_dir)
