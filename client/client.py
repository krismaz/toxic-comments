import click
import json
import requests


# Query the server for predictions for a given list of sentences
def get_predictions(host, port, sentences):
    url = host + ':' + str(port) + '/v1/models/model:predict'
    post_fields = {"instances": sentences}

    response = requests.post(url, data=json.dumps(post_fields))
    predictions = json.loads(response.text)['predictions']

    # Unpack inner lists
    predictions = [lst[0] for lst in predictions]

    return predictions


def apply_thresholding(predictions, threshold=0.5):
    return [x > threshold for x in predictions]


@click.command()
@click.option('--host', default='http://localhost')
@click.option('--port', default=8501)
@click.option('--input_file', required=True, type=click.Path())
def main(host, port, input_file):
    with open(input_file, 'r') as f:
        sentences = list(f)
        predictions = get_predictions(host, port, sentences)
        predictions = apply_thresholding(predictions)

        print(predictions)


if __name__ == "__main__":
    main()

