docker run -p 8501:8501 \
  --mount type=bind,source=$MODEL_PATH,target=/models/model \
  -e MODEL_NAME=model -t tensorflow/serving
